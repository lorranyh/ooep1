#ifndef FORMA_HPP
#define FORMA_HPP

class Forma{

protected:
	char** forma;

public:
	virtual char** getForma()=0;
	virtual void setForma(char** forma)=0;

};
#endif