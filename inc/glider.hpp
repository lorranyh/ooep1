#ifndef GLIDER_HPP
#define GLIDER_HPP
#include "forma.hpp"

class Glider: public Forma{

public: 
	Glider();
	char** getForma();
	void setForma(char** forma);

};

#endif