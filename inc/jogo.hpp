#ifndef JOGO_HPP
#define JOGO_HPP

class Jogo{

private:
	char** atual;
	char** futuro;


public:

	Jogo();
	void recebeGosperTotal(char** gosper,char** atual);
	void regras(int x, int y,char** atual, char** futuro);
	void atualizar(char** atual, char** futuro);
	void imprimir(char** atual);
	char** getAtual();
	void setAtual(char** atual);
	char** getFuturo();
	void setFuturo(char** futuro);
	int menu();

};
#endif
