#ifndef BLOCK_HPP
#define BLOCK_HPP
#include "forma.hpp"

class Block: public Forma{

public:
	Block();
	char** getForma();
	void setForma(char** forma);

};

#endif