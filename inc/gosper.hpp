#ifndef GOSPER1_HPP
#define GOSPER1_HPP
#include "forma.hpp"

class Gosper1: public Forma{

public:
	Gosper1();
	char** getForma();
	void setForma(char** forma);

};

#endif
