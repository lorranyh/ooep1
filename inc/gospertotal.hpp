#ifndef GOSPERTOTAL_HPP
#define GOSPERTOTAL_HPP
#include "forma.hpp"
#include "block.hpp"
#include "gosper.hpp"

class GosperTotal: public Forma{

public:
	GosperTotal();
	char** getForma();
	void setForma(char** forma);

};

#endif