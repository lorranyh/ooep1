#include <iostream>
#include <stdlib.h>
#include "jogo.hpp"

using namespace std;


Jogo::Jogo(){
	setAtual(getAtual());
	setFuturo(getFuturo());
}

int Jogo::menu(){
	int selecione;
	cout << "1) Iniciar" <<endl;
	cout << "2) Finalizar" <<endl;
	cin >> selecione;
	return selecione;
}

char** Jogo:: getAtual(){
	return atual;
}
void Jogo::setAtual(char** atual){
	int i,j;
	atual = (char**)malloc(30 * sizeof(char*));
 	this->atual = atual;
 	for (i = 0; i < 30; i++){
       atual[i] = (char*) malloc(60 * sizeof(char));
       this->atual[i] = atual[i];
       for (j = 0; j < 60; j++){ 
            atual[i][j] = '-';
            this->atual[i][j] = atual[i][j];
       }
  	}
	
}
char** Jogo:: getFuturo(){
	return futuro;
}
void Jogo:: setFuturo(char** futuro){
	int i,j;
	futuro = (char**)malloc(30 * sizeof(char*));
 	this->futuro = futuro;
 	for (i = 0; i < 30; i++){
       futuro[i] = (char*) malloc(60 * sizeof(char));
       this->futuro[i] = futuro[i];
       for (j = 0; j < 60; j++){ 
            futuro[i][j] = '-';
            this->futuro[i][j] = futuro[i][j];
       }
  	}
	
}

void Jogo:: imprimir(char** atual){

	int i,j;
	for (i=0; i<30; i++){
		for (j=0; j<60; j++){
			cout << atual[i][j];
	
		}
		cout << endl;
	}
}

void Jogo:: atualizar(char** atual, char** futuro){
	int i,j;
	for (i=0; i<30; i++){
		for (j=0; j<60; j++){
			atual[i][j] = futuro[i][j];
		}
		cout << endl;
	}

}
void Jogo::regras(int x, int y,char** atual, char** futuro){
	int aux=0;
	int prevX,prevY,posX,posY;
	prevX = x-1;
	posX = x+1;
	prevY = y-1;
	posY = y+1;

	if(x == 0){
		prevX = 29;
	}else if(x== 29){
		posX = 0;
	}
	if(y== 0){
		prevY= 59;
	}else if(y == 59) {
		posY = 0;
	}

	if(atual[prevX][prevY]== '$')
		aux++; 
	if(atual[prevX][y]== '$') 
		aux++;
	if(atual[prevX][posY]== '$') 
		aux++;
	if(atual[x][prevY]== '$') 
		aux++;
	if(atual[x][posY]== '$') 
		aux++;
	if(atual[posX][prevY]== '$') 
		aux++;
	if(atual[posX][y]== '$') 
		aux++;
	if(atual[posX][posY]== '$') 
		aux++;


	if(atual[x][y]== '$'){
		if(aux < 2 || aux > 3){
			futuro[x][y]= '-';
		}if(aux == 2){
			futuro[x][y] = '$';
		}
	}
	else{
		if(aux == 3) {
			futuro[x][y]= '$';
		}	
	}
}

void Jogo::recebeGosperTotal(char** gosper,char** atual){
	int i,j;
	for(i=0; i<9; i++){
		for(j=0; j<36; j++){
			atual[i][j]= gosper[i][j];
		}
	}
}


