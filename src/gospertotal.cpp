#include <iostream>
#include <stdlib.h>
#include "gospertotal.hpp"


GosperTotal::GosperTotal(){
	setForma(getForma());
}

char** GosperTotal:: getForma(){
	return forma;

}

void GosperTotal:: setForma(char** forma){
	int i,j;
	forma = (char**)malloc(9 * sizeof(char*));
 	this->forma = forma;
 	for (i = 0; i < 9; i++){
       forma[i] = (char*) malloc(36 * sizeof(char));
       this->forma[i] = forma[i];
       for (j = 0; j < 36; j++){ 
            this->forma[i][j] = forma[i][j]= '-';
       }
	}

	Block* block = new Block();
	Gosper1* gosper1 = new Gosper1();
	char** figuraBloco = block->getForma();
	char** figuraGosper1 = gosper1->getForma();
	
	for (i=0; i<2; i++){
		for(j=0; j<2; j++){
			this->forma[4+i][0+j]= forma[4+i][0+j]= figuraBloco[i][j];
			this->forma[2+i][34+j]=  forma[2+i][34+j]=figuraBloco[i][j];
		}
	}

	for(i=0; i<9; i++){
		for(j=0; j<15; j++){
			this->forma[0+i][10+j]= forma[0+i][10+j]= figuraGosper1[i][j];
		}
	}
}

	